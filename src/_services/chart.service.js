import config from 'config';

export const chartService = {
    getChartInfo,
    getStockInfo,
    getMultipleStockInfo,
    // getCompanyInfo,
    // serchCompanyInfo
};

function getChartInfo(eventname) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'}
    };
    //예 : 083660
    console.log('eventname:'+eventname);

    return fetch(`${config.apiUrl}/api/stock/`+eventname+'/highlowscore', requestOptions).then(handleResponse).then(response => {
        console.log(" @@@@@@@@@@@@ ", response);
        // console.log(" @@@@@@@@@@@@ ", config.apiUrl);
        return response;
    });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text;
        if (response.status !== 200) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        // console.log(data);
        return data;
    });
}

function getStockInfo(code){
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'}
    };
    //예 : 083660
    // console.log('url  => '+ `${config.apiUrl}/api/stock/`+code+'/highlow');
    return fetch(`${config.apiUrl}/api/stock/`+code+'/highlowscore', requestOptions).then(handleResponse);
}

function getMultipleStockInfo(codes){
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'}
    };

    console.log('codes', codes);
    return Promise.all([
        fetch(`${config.apiUrl}/api/stock/`+codes[0]+'/highlowscore', requestOptions),
        fetch(`${config.apiUrl}/api/stock/`+codes[1]+'/highlowscore', requestOptions),
        fetch(`${config.apiUrl}/api/stock/`+codes[2]+'/highlowscore', requestOptions),
    ]).then(([item1, item2, item3]) =>{
        let resultVal = [];
        console.log(" items1 => ", item1);
        console.log(" items2 => ", item2);
        console.log(" items3 => ", item3);
        return [item1, item2, item3];

        // item1.text().then(text1 =>{
        //     resultVal.push(text1);
        //     item2.text().then(text2 =>{
        //         resultVal.push(text2);
        //         item3.text().then(text3 =>{ 
        //             resultVal.push(text3);
        //             console.log(" resultVal => ", resultVal);
        //         })
        //     })
        // });
    })
}

function getStockMasterInfo(code){
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'}
    };
    let selectiveMaster = "";
    let masterUrl = "https://sandbox-apigw.koscom.co.kr/v2/market/stocks/"+marketCode+"/"+stockCode+"/selectiveMaste?"+selectiveMaster;

    return fetch(masterUrl, requestOptions).then(handleResponse);
}